IDIR =inc
CC=gcc -O3
CFLAGS=-I$(IDIR)

OUT_DIR = bin
ODIR = obj
SDIR = src

LIBS=

#
# Build for Windows
#
ifeq ($(WINDOWS),1)
CFLAGS += -DWINDOWS_BUILD
endif

_OBJ = main.o transaction.o account.o utils.o cli.o data_chain.o interpreter.o fi_cli.o calendar.o message.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

$(ODIR)/%.o: $(SDIR)/%.c
	$(CC) $(CFLAGS) -c -o $@ $<

$(OUT_DIR)/fi_track: $(OBJ)
	$(CC) -Wall -o $@ $^ $(CFLAGS) $(LIBS)

.PHONY: clean

clean:
	rm -f $(ODIR)/*.o *~ core $(INCDIR)/*~ 
