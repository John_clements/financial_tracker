#include "stdio.h"
#include "stdint.h"
#include "string.h"
#include "time.h"

#include "utils.h"

// Prints divider line
void print_divider(char* title, uint8_t wall_len)
{
    uint32_t    i           = 0;
    uint8_t     title_len   = 0xFF;

    if (title == NULL)
        title = "";

    do
    {
        title_len++;
    } while ((title[title_len] != '\0') || (title_len>= 0xFF));

    for (i = 0; i < ((wall_len >> 1) - ((title_len+2) >>1)); i++)
        printf("-");

    if (title == "")
        printf("--");
    else
        printf(" %s ", title);

    for (i = 0; i < ((wall_len >> 1) - ((title_len+2) >>1)); i++)
        printf("-");

    if (!(title_len % 2))
        printf("-");

    printf("\n");
}

/* ---- TODO ----
char* get_date_string(struct_date date)
{
    char* date_str = (char*)malloc(64);

    memset(date_str, 0, 64);

}*/

uint8_t get_days_in_month(uint16_t year, uint8_t month)
{
    if (month == 1)
        return 31;
    else if (month == 2)
    {
        if ((year % 4) == 0)
            return 29;
        return 28;
    }
    else if (month == 3)
        return 31;
    else if (month == 4)
        return 30;
    else if (month == 5)
        return 31;
    else if (month == 6)
        return 30;
    else if (month == 7)
        return 31;
    else if (month == 8)
        return 31;
    else if (month == 9)
        return 30;
    else if (month == 10)
        return 31;
    else if (month == 11)
        return 30;
    else if (month == 12)
        return 31;

    return 0;
}

// return true if (time_a >= time_b)
uint32_t time_comparison(struct_date time_a, struct_date time_b)
{
    return (get_time_stamp(time_a) >= get_time_stamp(time_b));
}

uint32_t get_difference(uint32_t a, uint32_t b)
{
    if (a > b)
        return a - b;
   return b - a;
}

struct_date get_time_difference(struct_date time_a, struct_date time_b)
{
    struct_date time_diff = { 0 };

    time_diff.year  = get_difference(time_a.year, time_b.year);
    time_diff.month = (uint8_t)get_difference((uint32_t)time_a.month, (uint32_t)time_b.month);
    time_diff.day   = (uint8_t)get_difference((uint32_t)time_a.day, (uint32_t)time_b.day);

    if ((time_a.year > time_b.year) && (time_b.month > time_a.month))
    {
        time_diff.year--;
        time_diff.month = time_b.month - time_a.month;
    }
    else if ((time_b.year > time_a.year) && (time_a.month > time_b.month))
    {
        time_diff.year--;
        time_diff.month = time_a.month - time_b.month;
    }

    if ((time_a.month > time_b.month) && (time_b.day > time_a.day))
    {
        time_diff.month--;
        time_diff.day = time_b.day - time_a.day;
    }
    else if ((time_b.month > time_a.month) && (time_a.day > time_b.day))
    {
        time_diff.month--;
        time_diff.day = time_a.day - time_b.day;
    }

    return time_diff;
}

uint32_t get_total_months(struct_date time)
{
    return (time.year * 12) + time.month;
}

uint32_t get_months_difference(struct_date time_a, struct_date time_b)
{
    struct_date time_diff = { 0 };

    time_diff.year  = get_difference(time_a.year, time_b.year);
    time_diff.month = (uint8_t)get_difference((uint32_t)time_a.month, (uint32_t)time_b.month);
    time_diff.day   = (uint8_t)get_difference((uint32_t)time_a.day, (uint32_t)time_b.day);

    if ((time_a.year > time_b.year) && (time_b.month > time_a.month))
    {
        time_diff.year--;
        time_diff.month = time_b.month - time_a.month;
    }
    else if ((time_b.year > time_a.year) && (time_a.month > time_b.month))
    {
        time_diff.year--;
        time_diff.month = time_a.month - time_b.month;
    }

    return get_total_months(time_diff);
}

struct_date build_date_struct(uint16_t year, uint8_t month, uint8_t day)
{
    struct_date date = {0};

    date.year   = year;
    date.month  = month;
    date.day    = day;

    return date;
}

uint64_t get_time_stamp(struct_date date)
{
    uint64_t time_stamp = 0;

    time_stamp = ((((uint64_t)date.year)     << 40) |
                  (((uint64_t)date.month)    << 32) |
                  (((uint64_t)date.day)      << 24));

    return time_stamp;
}

struct_date get_date_struct(uint64_t time_stamp)
{
    struct_date date = {0};

    date.year   = ((uint16_t)(time_stamp >> 40)) & 0xFFFF;
    date.month  = ((uint8_t)(time_stamp  >> 32)) & 0x00FF;
    date.day    = ((uint8_t)(time_stamp  >> 24)) & 0x00FF;

    return date;
}

struct_date get_current_date()
{
    int year    = 0;
    int month   = 0;
    int day     = 0;

    time_t t        = time(NULL);
    struct tm tm    = *localtime(&t);

    year    = (int)tm.tm_year + 1900;
    month   = (int)tm.tm_mon  + 1;
    day     = (int)tm.tm_mday;

    return build_date_struct((uint16_t)year, (uint8_t)month, (uint8_t)day);
}

void print_indent(uint32_t cnt)
{
    for (uint32_t i = 0; i < INDENT_SIZE*cnt; i++)
        printf(" ");
}

void get_user_text_input(char* output_buf, uint32_t output_buf_size)
{
    char        c = 0;
    uint32_t    n = 0;

    if (output_buf == NULL)
        return;

    while (((c = getchar()) != EOF) && (n < output_buf_size))
    {
        if ((c == '\n') && (n == 0))
            continue;
        else if ((c == '\n') && (n > 0))
            break;

        output_buf[n] = c;
        n++;
    }

    if ((c == '\n') && (n < (output_buf_size-1)))
    {
        output_buf[n] = '\0';
    }
}

int get_user_int_input()
{
    int input_result    = 0;
    int integer         = 0;

    integer      = -1;
    input_result = scanf("%d", &integer);

    if (input_result == EOF)
        return -1;

    if (input_result == 0)
        while (fgetc(stdin) != '\n');

    return integer;
}

// return 1 for y
// return 0 for n
int get_user_yes_or_no()
{
    char c = 0;

    do
    {
        c = getchar();
    } while ((c != 'y') && (c != 'n'));

    if (c == 'y')
        return 1;

    return 0;
}

// Print the byte contents at a memory address in a table format
void print_mem_table(uint8_t* mem, uint32_t mem_size, uint32_t line_width)
{
    printf("\n  TABLE  |");

    for (int i = 0; i < line_width; i++)
        printf(" %02X", i);

    printf("\n---------|");

    for (int i = 0; i < line_width; i++)
        printf("---");

    printf("\n%08X | ", 0);

    for (int i = 0; i < mem_size; i++)
    {
        printf("%02X ", mem[i]);
        if ((i+1)%line_width == 0)
            printf("\n%08X | ",i+1);
    }

    printf("\n");
}
