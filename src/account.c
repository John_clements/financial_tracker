#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#include "account.h"

int is_ac_record_struct_valid(struct_ac_record* ac_record)
{
    if (ac_record == NULL)
        return 0;

    if (is_fi_header_valid(&ac_record->header) == 0)
        return 0;

    if (ac_record->header.fi_type != FI_TYPE_ACCOUNT)
        return 0;

    return 1;
}

int is_virt_ac_record_struct_valid(struct_virt_ac_record* virt_ac_record)
{
    if (virt_ac_record == NULL)
        return 0;

    if (is_fi_header_valid(&virt_ac_record->header) == 0)
        return 0;

    if (virt_ac_record->header.fi_type != FI_TYPE_VIRT_ACCOUNT)
        return 0;

    return 1;
}

int is_account_added(struct_data_chain* data_chain, char* name)
{
    int     ret     = 0;
    void*   data    = NULL;

    struct_ac_record* ac_record = NULL;

    if ((data_chain == NULL) || (name == NULL))
        return 0;

    for (uint32_t i = 0; i < chain_link_count(data_chain); i++)
    {
        data = get_link_data(get_chain_link(data_chain, i), NULL);

        if (is_ac_record_struct_valid((struct_ac_record*)data) ||
            is_virt_ac_record_struct_valid((struct_virt_ac_record*)data))
        {
            ac_record = (struct_ac_record*)data;

            if (strncmp(ac_record->name, name, AC_NAME_SIZE) == 0)
            {
                if (ac_record->record_type == AC_RECORD_ADD)
                    ret = 1;
                else if (ac_record->record_type == AC_RECORD_REMOVE)
                    ret = 0;
            }
        }
    }


    return ret;
}

void add_virt_account(struct_data_chain* data_chain,
                      char*              name,
                      char*              parent_name,
                      int                monthly_request,
                      int                capacity,
                      enum_fill_type     required_fill,
                      struct_date        date)
{
    struct_virt_ac_record* virt_ac_record = NULL;

    if ((data_chain == NULL) || (name == NULL) || (parent_name == NULL))
        return;

    virt_ac_record = (struct_virt_ac_record*)malloc(sizeof(struct_virt_ac_record));

    memset((void*)virt_ac_record, 0, sizeof(struct_virt_ac_record));

    strncpy(virt_ac_record->name,           name,           AC_NAME_SIZE);
    strncpy(virt_ac_record->parent_name,    parent_name,    AC_NAME_SIZE);

    virt_ac_record->record_type     = AC_RECORD_ADD;
    virt_ac_record->monthly_request = monthly_request;
    virt_ac_record->capacity        = capacity;
    virt_ac_record->required_fill   = required_fill;
    virt_ac_record->date            = date;

    set_fi_header(&virt_ac_record->header, FI_TYPE_VIRT_ACCOUNT);

    add_chain_link(data_chain, (void*)virt_ac_record, sizeof(struct_virt_ac_record));
}

void process_account_record(struct_data_chain*  data_chain,
                            char*               name,
                            enum_record_type    type,
                            struct_date         date)
{
    struct_ac_record* ac_record = NULL;

    if ((data_chain == NULL) || (name == NULL))
        return;

    ac_record = (struct_ac_record*)malloc(sizeof(struct_ac_record));

    memset((void*)ac_record, 0, sizeof(struct_ac_record));

    strncpy(ac_record->name, name, AC_NAME_SIZE);

    ac_record->record_type  = type;
    ac_record->date         = date;

    set_fi_header(&ac_record->header, FI_TYPE_ACCOUNT);

    add_chain_link(data_chain, (void*)ac_record, sizeof(struct_ac_record));
}

void add_account(struct_data_chain* data_chain, char* name, struct_date date)
{
    struct_ac_record* ac_record = NULL;

    if ((data_chain == NULL) || (name == NULL))
        return;

    process_account_record(data_chain, name, AC_RECORD_ADD, date);
}

void delete_account(struct_data_chain* data_chain, char* name, struct_date date)
{
    struct_ac_record* ac_record = NULL;

    if ((data_chain == NULL) || (name == NULL))
        return;

    process_account_record(data_chain, name, AC_RECORD_REMOVE, date);
}

void print_virt_account_record(struct_virt_ac_record* virt_ac_record)
{
    if (virt_ac_record == NULL)
        return;

    PRINT_DIVIDER("");
    printf(KMAG"Virtual Account Record\n"KNRM);
    printf("Name:              %s\n", virt_ac_record->name);
    printf("Parent Name:       %s\n", virt_ac_record->parent_name);
    printf("Date:              %4d-%02d-%02d\n", virt_ac_record->date.year, virt_ac_record->date.month, virt_ac_record->date.day);

    if (virt_ac_record->record_type == AC_RECORD_REMOVE)
        printf("Type:              "KRED"Remove"KNRM"\n");
    else if (virt_ac_record->record_type == AC_RECORD_ADD)
        printf("Type:              "KGRN"Add"KNRM"\n");
    else if (virt_ac_record->record_type == AC_RECORD_UPDATE)
        printf("Type:              "KGRN"Add"KNRM"\n");
    else
        printf("Type:              Unknown\n");

    printf("Monthly Request:   %d\n", virt_ac_record->monthly_request);
    printf("Capacity:          %d\n", virt_ac_record->capacity);

    if (virt_ac_record->required_fill == FILL_TYPE_REQUIRED)
        printf("Required Fill:     "KRED"Required"KNRM"\n");
    else if (virt_ac_record->required_fill == FILL_TYPE_NOT_REQUIRED)
        printf("Required Fill:     "KGRN"Not Required"KNRM"\n");
    else
        printf("Required Fill:     Unknown\n");

}

void print_account_record(struct_ac_record* ac_record)
{
    if (ac_record == NULL)
        return;

    PRINT_DIVIDER("");
    printf(KMAG"Account Record\n"KNRM);
    printf("Name:   %s\n", ac_record->name);
    printf("Date:   %4d-%02d-%02d\n", ac_record->date.year, ac_record->date.month, ac_record->date.day);

    if (ac_record->record_type == AC_RECORD_REMOVE)
        printf("Type:   "KRED"Remove"KNRM"\n");
    else if (ac_record->record_type == AC_RECORD_ADD)
        printf("Type:   "KGRN"Add"KNRM"\n");
    else
        printf("Type:   Unknown\n");
}

void print_all_accounts(struct_data_chain* data_chain)
{
    void* data = NULL;

    if (data_chain == NULL)
        return;

    for (uint32_t i = 0; i < chain_link_count(data_chain); i++)
    {
        data = get_link_data(get_chain_link(data_chain, i), NULL);

        if (is_ac_record_struct_valid((struct_ac_record*)data))
            print_account_record((struct_ac_record*)data);
        else if (is_virt_ac_record_struct_valid((struct_virt_ac_record*)data))
            print_virt_account_record((struct_virt_ac_record*)data);
    }
}
