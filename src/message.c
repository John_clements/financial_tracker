#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#include "message.h"

int is_msg_record_struct_valid(struct_msg_record* msg_record)
{
    if (msg_record == NULL)
        return 0;

    if (is_fi_header_valid(&msg_record->header) == 0)
        return 0;

    if (msg_record->header.fi_type != FI_TYPE_MESSAGE)
        return 0;

    return 1;
}

struct_msg_record* decode_message_record(void* chain_link_data)
{
    struct_msg_record* msg_record       = NULL;
    struct_msg_record* chain_msg_record = (struct_msg_record*)chain_link_data;

    if (!is_msg_record_struct_valid(chain_msg_record))
        return NULL;

    msg_record = (struct_msg_record*)malloc(sizeof(struct_msg_record));

    memcpy((void*)msg_record, chain_link_data, sizeof(struct_msg_record));

    msg_record->message = (char*)&chain_msg_record->message;

    return msg_record;
}

void add_message_record(struct_data_chain* data_chain,
                        char*              sender_name,
                        struct_date        date,
                        uint32_t           message_size,
                        char*              message)
{
    struct_msg_record* msg_record  = NULL;
    uint32_t           record_size = sizeof(struct_msg_record) + message_size - sizeof(char*);

    if ((data_chain == NULL) || (sender_name == NULL))
        return;

    if ((message == NULL) && (message_size != 0))
        return;

    msg_record = (struct_msg_record*)malloc(record_size);

    memset((void*)msg_record, 0, record_size);

    strncpy(msg_record->sender_name, sender_name, MSG_NAME_SIZE);

    msg_record->message_size   = message_size;
    msg_record->date           = date;

    memcpy((void*)(&msg_record->message), message, message_size);

    set_fi_header(&msg_record->header, FI_TYPE_MESSAGE);

    add_chain_link(data_chain, (void*)msg_record, record_size);
}

void print_message_record(struct_msg_record* msg_record)
{
    if (msg_record == NULL)
        return;

    PRINT_DIVIDER("");
    printf(KMAG"Message Record\n"KNRM);
    printf("Sender:       %s\n", msg_record->sender_name);
    printf("Date:         %4d-%02d-%02d\n", msg_record->date.year, msg_record->date.month, msg_record->date.day);
    printf("Message size: %d\n", msg_record->message_size);

    if (msg_record->message_size > 0)
    {
        printf("Message:      %s\n", msg_record->message);
    }
    else
    {
        printf("Message:      Null\n");
    }
}
