#include "stdio.h"
#include "stdint.h"
#include "stdlib.h"
#include "string.h"

#include "main.h"
#include "transaction.h"
#include "account.h"
#include "interpreter.h"
#include "message.h"
#include "cli.h"

struct_data_chain* build_test_data_structure()
{
    struct_data_chain* data_chain = init_data_chain();

    add_account(data_chain, "savings",      build_date_struct(2018, 01, 01));
    add_account(data_chain, "checking",     build_date_struct(2018, 01, 01));
    add_account(data_chain, "tfsa",         build_date_struct(2018, 01, 01));
    add_account(data_chain, "rrsp",         build_date_struct(2018, 01, 01));
    add_account(data_chain, "gh corp",      build_date_struct(2018, 01, 01));
    add_account(data_chain, "cash reserve", build_date_struct(2018, 01, 01));

    add_virt_account(data_chain, "gh initial purchase", "gh corp",  0, AC_MAX_CAPACITY, FILL_TYPE_NOT_REQUIRED, build_date_struct(2018, 01, 01));
    add_virt_account(data_chain, "gh current expenses", "gh corp",  0, AC_MAX_CAPACITY, FILL_TYPE_NOT_REQUIRED, build_date_struct(2018, 01, 01));
    add_virt_account(data_chain, "gh income reserve",   "gh corp",  0, AC_MAX_CAPACITY, FILL_TYPE_NOT_REQUIRED, build_date_struct(2018, 01, 01));

    add_transaction(data_chain, "cash reserve", "initial input", 300, build_date_struct(2018, 02, 01));

    add_transaction(data_chain, "gh initial purchase", "down payment", 10000, build_date_struct(2018, 8, 1));
    add_transaction(data_chain, "gh initial purchase", "payment",      500,   build_date_struct(2018, 9, 1));
    add_transaction(data_chain, "gh initial purchase", "payment",      500,   build_date_struct(2018, 9, 1));
/*
    add_virt_account(data_chain, "rent",        "checking",  500, 10000,             FILL_TYPE_REQUIRED,     build_date_struct(2018, 01, 01));
    add_virt_account(data_chain, "personal",    "checking",  600, AC_MAX_CAPACITY,   FILL_TYPE_NOT_REQUIRED, build_date_struct(2018, 01, 01));
    add_virt_account(data_chain, "john priv",   "personal",  300, AC_MAX_CAPACITY,   FILL_TYPE_NOT_REQUIRED, build_date_struct(2018, 01, 01));
    add_virt_account(data_chain, "john->snacks","john priv", 40,  100,               FILL_TYPE_NOT_REQUIRED, build_date_struct(2018, 01, 01));
    add_virt_account(data_chain, "cuixia priv", "personal",  300, AC_MAX_CAPACITY,   FILL_TYPE_NOT_REQUIRED, build_date_struct(2018, 01, 01));
    add_virt_account(data_chain, "car",         "checking",  540, 5500,              FILL_TYPE_REQUIRED,     build_date_struct(2018, 01, 01));

    add_transaction(data_chain, "checking", "Paycheck", 2400,  build_date_struct(2018, 02, 01));
    add_transaction(data_chain, "rent",     "rent",     -500,  build_date_struct(2018, 02, 01));
    add_transaction(data_chain, "car",      "car",      -600,  build_date_struct(2018, 02, 01));

    add_transaction(data_chain, "checking", "Paycheck", 2400,  build_date_struct(2018, 03, 01));
    add_transaction(data_chain, "rent",     "rent",     -500,  build_date_struct(2018, 03, 01));
    add_transaction(data_chain, "car",      "car",      -600,  build_date_struct(2018, 03, 01));

    add_transaction(data_chain, "checking", "Paycheck", 2400,  build_date_struct(2018, 04, 01));
    add_transaction(data_chain, "rent",     "rent",     -500,  build_date_struct(2018, 04, 01));
    add_transaction(data_chain, "car",      "car",      -600,  build_date_struct(2018, 04, 01));

    add_transaction(data_chain, "checking", "Paycheck", 2400,  build_date_struct(2018, 05, 01));
*/
    return data_chain;
}

int is_fi_header_valid(fi_header* header)
{
    if (header == NULL)
        return 0;

    if (header->fi_uuid != FI_TRACKER_UUID)
        return 0;

    return 1;
}

void set_fi_header(fi_header* header, enum_fi_type fi_type)
{
    if (header == NULL)
        return;

    memset((void*)header, 0, sizeof(fi_header));

    header->fi_uuid = FI_TRACKER_UUID;
    header->version = FI_TRACKER_VERSION;
    header->fi_type = fi_type;
}

void print_fi_data_chain(struct_data_chain* data_chain)
{
    void* data = NULL;

    struct_msg_record* msg_record = NULL;

    if (data_chain == NULL)
        return;

    for (uint32_t i = 0; i < chain_link_count(data_chain); i++)
    {
        data = get_link_data(get_chain_link(data_chain, i), NULL);

        if (is_ac_record_struct_valid((struct_ac_record*)data))
            print_account_record((struct_ac_record*)data);
        else if (is_virt_ac_record_struct_valid((struct_virt_ac_record*)data))
            print_virt_account_record((struct_virt_ac_record*)data);
        else if (is_tr_struct_valid((struct_tr*)data))
            print_transaction((struct_tr*)data);
        else if (is_inter_tr_struct_valid((struct_inter_tr*)data))
            print_inter_transaction((struct_inter_tr*)data);
        else if (is_msg_record_struct_valid((struct_msg_record*)data))
        {
            msg_record = decode_message_record(data);

            if (msg_record != NULL)
            {
                print_message_record(msg_record);

                free(msg_record);
            }
        }
        else
        {
            PRINT_DIVIDER("");
            printf(KRED"Unkown Record\n"KNRM);
            printf("Index:      %d\n",        get_chain_link(data_chain, i)->index);
            printf("Data size:  %d Bytes\n",  get_chain_link(data_chain, i)->data_size);
        }
    }
}

void free_data_chain_data(struct_data_chain* data_chain)
{
    void* data = NULL;

    if (data_chain == NULL)
        return;

    for (uint32_t i = 0; i < chain_link_count(data_chain); i++)
    {
        data = get_link_data(get_chain_link(data_chain, i), NULL);

        if (data != NULL)
            free(data);
    }
}

char* get_fi_version_string()
{
    uint32_t major   = (FI_TRACKER_VERSION & 0xFF000000) >> 24;
    uint32_t minor   = (FI_TRACKER_VERSION & 0x00FF0000) >> 16;
    uint32_t year    = (FI_TRACKER_VERSION & 0x0000FF00) >> 8;
    uint32_t month   = (FI_TRACKER_VERSION & 0x000000FF) >> 0;
    char*    version = (char*)malloc(32*sizeof(char));

    sprintf(version, "%d.%d.%d.%d", major, minor, year, month);

    return version;
}

void main(int argc, char* argv[])
{
    struct_data_chain* data_chain  = NULL;
    char*              version     = get_fi_version_string();

    if (version != NULL)
    {
        printf("Financial Tracker Version: %s\n", version);
        free(version);
    }

    data_chain = import_data_chain("data_chain_save");

    if (data_chain == NULL)
        data_chain = init_data_chain();

    fi_tracker_cli(data_chain);

    export_data_chain(data_chain, "data_chain_save");

    free_data_chain_data(data_chain);

    free_data_chain(data_chain);

    DIVIDER("END");
}
