#include "stdio.h"
#include "stdint.h"
#include "stdlib.h"
#include "string.h"

#include "data_chain.h"

struct_data_chain* init_data_chain()
{
    struct_data_chain* data_chain = (struct_data_chain*)malloc(sizeof(struct_data_chain));

    memset((void*)data_chain, 0, sizeof(struct_data_chain));

    return data_chain;
}

uint32_t chain_link_count(struct_data_chain* data_chain)
{
    if (data_chain == NULL)
        return 0;

    return data_chain->count;
}

void update_chain_hash(struct_data_chain* data_chain)
{
    // TODO: Implement separate process to build array
}

struct_chain_link* get_chain_link(struct_data_chain* data_chain, uint32_t index)
{
    struct_chain_link* chain_link = NULL;

    if (data_chain == NULL)
        return NULL;

     if (data_chain->chain_start == NULL)
        return NULL;

    if (data_chain->is_hash_up_to_date)
        return data_chain->chain_hash[index];
    else
        update_chain_hash(data_chain);

    chain_link = data_chain->chain_start;

    do
    {
        if (chain_link->index == index)
            break;
        chain_link = chain_link->next;
    } while (chain_link != NULL);

    return chain_link;
}

struct_chain_link* get_end_of_chain(struct_data_chain* data_chain)
{
    if (data_chain == NULL)
        return NULL;

    return data_chain->chain_end;
}

void* get_link_data(struct_chain_link* chain_link, uint32_t* data_size)
{
    if (chain_link == NULL)
        return NULL;

    if (data_size != NULL)
        *data_size = chain_link->data_size;

    return chain_link->data;
}

struct_chain_link* construct_chain_link(void* data, uint32_t data_size)
{
    struct_chain_link* link = (struct_chain_link*)malloc(sizeof(struct_chain_link));

    memset((void*)link, 0, sizeof(struct_chain_link));

    link->data      = data;
    link->data_size = data_size;

    return link;
}

// Connects link b to the end of link a
void connect_chain_links(struct_chain_link* a, struct_chain_link* b)
{
    a->next = b;
    b->prev = a;

    b->index = a->index + 1;
}

void add_chain_link(struct_data_chain* data_chain, void* data, uint32_t data_size)
{
    struct_chain_link* new_link = NULL;

    if (data_chain == NULL)
        return;

    new_link = construct_chain_link(data, data_size);

    if (data_chain->chain_start == NULL)
        data_chain->chain_start = new_link;
    else
        connect_chain_links(data_chain->chain_end, new_link);

    data_chain->chain_end = new_link;

    data_chain->count++;
}

void export_data_chain(struct_data_chain* data_chain, char* path)
{
    FILE*               fp      = NULL;
    struct_chain_link*  link    = NULL;

    if ((data_chain == NULL) || (path == NULL))
        return;

    fp = fopen(path, "wb");

    if (fp != NULL)
    {
        fwrite((void*)&data_chain->count, sizeof(uint32_t), 1, fp);

        for (uint32_t i = 0; i < chain_link_count(data_chain); i++)
        {
            link = get_chain_link(data_chain, i);

            fwrite((void*)&link->data_size, sizeof(uint32_t), 1, fp);
            fwrite(link->data,              link->data_size, 1, fp);
        }

        fclose(fp);
    }
    else
    {
        // return error
    }

    // return success
}

struct_data_chain* import_data_chain(char* path)
{
    FILE*               fp          = NULL;
    struct_data_chain*  data_chain  = NULL;
    uint32_t            link_count  = 0;
    void*               data        = NULL;
    uint32_t            data_size   = 0;

    if (path == NULL)
        return NULL;

    fp = fopen(path, "rb");

    if (fp == NULL)
        return NULL;

    data_chain = init_data_chain();

    if (fread((void*)&link_count, 1,  sizeof(uint32_t), fp) != sizeof(uint32_t))
    {
        // DO ERROR
    }

    for (uint32_t i = 0; i < link_count; i++)
    {
        if (fread((void*)&data_size, 1,  sizeof(uint32_t), fp) != sizeof(uint32_t))
        {
            // DO ERROR
        }

        data = malloc(data_size);
        memset(data, 0, data_size);

        if (fread(data, 1, data_size, fp) != data_size)
        {
            // DO ERROR
        }

        add_chain_link(data_chain, data, data_size);
    }

    fclose(fp);
    return data_chain;
}

void free_data_chain(struct_data_chain* data_chain)
{
    struct_chain_link* chain_link       = NULL;
    struct_chain_link* chain_link_next  = NULL;

    if (data_chain == NULL)
        return;

    chain_link = data_chain->chain_start;

    while (chain_link != NULL)
    {
        chain_link_next = chain_link->next;

        free(chain_link);

        chain_link = chain_link_next;
    }
}
