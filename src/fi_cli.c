#include "stdio.h"
#include "stdint.h"
#include "stdlib.h"
#include "string.h"

#include "main.h"
#include "transaction.h"
#include "account.h"
#include "interpreter.h"
#include "calendar.h"
#include "message.h"
#include "cli.h"

int cli_print_data_chain(void* shared_context_memory, char* option_name)
{
    struct_data_chain* data_chain = (struct_data_chain*)shared_context_memory;

    if (data_chain == NULL)
        return -1;

    print_fi_data_chain(data_chain);

    return 0;
}

int cli_print_accounts(void* shared_context_memory, char* option_name)
{
    struct_data_chain* data_chain = (struct_data_chain*)shared_context_memory;

    if (data_chain == NULL)
        return -1;

    print_all_accounts(data_chain);

    return 0;
}

int cli_print_transactions(void* shared_context_memory, char* option_name)
{
    struct_data_chain* data_chain = (struct_data_chain*)shared_context_memory;

    if (data_chain == NULL)
        return -1;

    print_all_transactions(data_chain);

    return 0;
}

void cli_print_account_data(struct_data_chain* data_chain, uint8_t detailed)
{
    struct_ac*          ac          = NULL;
    struct_calendar*    calendar    = NULL;

    if (data_chain == NULL)
        return ;

    calendar    = build_calendar(data_chain);
    ac          = process_calendar(calendar);

    if (detailed)
        print_account_detailed(ac, 0);
    else
        print_account_balance(ac, 0);

    free_calendar(calendar);
    free_account(ac);
}

int cli_print_account_balances(void* shared_context_memory, char* option_name)
{
    struct_data_chain*  data_chain  = (struct_data_chain*)shared_context_memory;

    if (data_chain == NULL)
        return -1;

    cli_print_account_data(data_chain, 0);

    return 0;
}

int cli_print_account_detailed(void* shared_context_memory, char* option_name)
{
    struct_data_chain*  data_chain  = (struct_data_chain*)shared_context_memory;

    if (data_chain == NULL)
        return -1;

    cli_print_account_data(data_chain, 1);

    return 0;
}

void cli_spacing_funcion()
{
    PRINT_DIVIDER("");
}

int is_year_within_valid_range(int year)
{
    return 1;
}

int is_month_within_valid_range(int month)
{
    if ((month > 0) && (month <= 12))
        return 1;
    return 0;
}

struct_date get_time_from_user()
{
    int year    = 0;
    int month   = 0;

    struct_date current_time = {0};

    printf("\nUse current time (y or n): ");

    if (get_user_yes_or_no())
    {
        current_time = get_current_date();

        year    = current_time.year;
        month   = current_time.month;

        printf("\n");
    }
    else
    {
        do
        {
            printf("\nSet transaction year:\n");
            year = get_user_int_input();
        } while (!is_year_within_valid_range(year));

        do
        {
            printf("\nSet transaction month (%02d <= month <= %02d):\n", 1, 12);
            month = get_user_int_input();
        } while (!is_month_within_valid_range(month));
    }

    return build_date_struct((uint16_t)year, (uint8_t)month, 1);
}

int cli_process_add_transaction_account(void* shared_context_memory, char* option_name)
{
    struct_data_chain*  data_chain          = (struct_data_chain*)shared_context_memory;
    char                desc[TR_DESC_SIZE]  = { 0 };
    int                 amount              = 0;
    struct_date         date                = { 0 };

    if (data_chain == NULL)
        return -1;

    printf("Write transaction description:\n");
    get_user_text_input(desc, TR_DESC_SIZE);

    printf("\nSet transaction ammount:\n");
    amount = get_user_int_input();

    date = get_time_from_user();

    printf("\nPending transaction:\n");
    printf("account:     %s\n", option_name);
    printf("description: %s\n", desc);
    printf("ammount:     %d\n", amount);
    printf("date:        %04d-%02d\n", date.year, date.month);

    printf("\nAre you sure you want to add this transaction (y or n): ");

    if (get_user_yes_or_no())
    {
        add_transaction(data_chain,
                        option_name,
                        desc,
                        amount,
                        date);
    }
    printf("\n");

    return -1;
}

void build_account_option_list(struct_cli_option_list* option_list, struct_ac* ac, cli_function function)
{
    int option_list_index   = 0;

    if ((option_list == NULL) || (ac == NULL))
        return;

    option_list_index = add_option_to_list(option_list, ac->name, CLI_FUNCTION_CALL);
    set_option_function(option_list, option_list_index, function);

    if (ac->child_account != NULL)
        build_account_option_list(option_list, ac->child_account, function);

    build_account_option_list(option_list, ac->next_account, function);

}

int cli_add_account(void* shared_context_memory, char* option_name)
{
    struct_data_chain*  data_chain          = (struct_data_chain*)shared_context_memory;
    char                name[AC_NAME_SIZE]  = { 0 };
    struct_date         date                = { 0 };

    if (data_chain == NULL)
        return -1;

    printf("Input account name:\n");
    get_user_text_input(name, AC_NAME_SIZE);

    date = get_time_from_user();

    printf("\nPending account record to add:\n");
    printf("account:     %s\n", name);
    printf("date:        %04d-%02d\n", date.year, date.month);

    printf("\nAre you sure you want to add this account (y or n): ");

    if (get_user_yes_or_no())
    {
        if (is_account_added(data_chain, name))
        {
            printf("Account "KRED"%s"KNRM" already exists", name);
        }
        else
        {
            add_account(data_chain, name, date);
        }
    }
    printf("\n");

    return 0;
}

int cli_process_add_virt_account(void* shared_context_memory, char* option_name)
{
    struct_data_chain*  data_chain          = (struct_data_chain*)shared_context_memory;
    char                name[AC_NAME_SIZE]  = { 0 };
    struct_date         date                = { 0 };
    int                 monthly_request     = 0;
    int                 capacity            = AC_MAX_CAPACITY;
    enum_fill_type      required_fill       = FILL_TYPE_NOT_REQUIRED;

    if (data_chain == NULL)
        return -1;

    printf("Input account name:\n");
    get_user_text_input(name, AC_NAME_SIZE);

    printf("\nInput accounts monthly request:\n");
    monthly_request = get_user_int_input();

    printf("\nDoes account have a maximum capacity (y or n): ");

    if (get_user_yes_or_no())
    {
        printf("\nInput accounts maximum capacity:\n");
        capacity = get_user_int_input();
    }

    printf("\nIs account monthly request required to be filled (y or n): ");

    if (get_user_yes_or_no())
    {
        required_fill = FILL_TYPE_REQUIRED;
    }

    date = get_time_from_user();

    printf("\nPending account record to delete:\n");
    printf("parent account: %s\n", option_name);
    printf("account:        %s\n", name);

    if (capacity == AC_MAX_CAPACITY)
        printf("capacity:       infinity\n");
    else
        printf("capacity:       %d\n", capacity);

    if (required_fill == FILL_TYPE_REQUIRED)
        printf("fill type:      required\n");
    else
        printf("fill type:      not-required\n");

    printf("date:           %04d-%02d\n", date.year, date.month);

    printf("\nAre you sure you want to add this account (y or n): ");

    if (get_user_yes_or_no())
    {
        add_virt_account(data_chain,
                         name,
                         option_name,
                         monthly_request,
                         capacity,
                         required_fill,
                         date);
    }
    printf("\n");

    return -1;
}

int cli_process_delete_account(void* shared_context_memory, char* option_name)
{
    struct_data_chain*  data_chain  = (struct_data_chain*)shared_context_memory;
    struct_date         date        = { 0 };

    if (data_chain == NULL)
        return -1;

    date = get_time_from_user();

    printf("\nPending account record to delete:\n");
    printf("account:     %s\n", option_name);
    printf("date:        %04d-%02d\n", date.year, date.month);

    printf("\nAre you sure you want to delete this account (y or n): ");

    if (get_user_yes_or_no())
    {
        delete_account(data_chain, option_name, date);
    }
    printf("\n");

    return -1;
}

void setup_account_option_list(void* shared_context_memory, cli_function function)
{
    struct_cli_option_list* option_list = NULL;
    struct_data_chain*      data_chain  = (struct_data_chain*)shared_context_memory;
    struct_ac*              ac          = NULL;
    struct_calendar*        calendar    = NULL;

    if (data_chain == NULL)
        return;

    option_list = create_option_list("Select account");

    set_spacing_function(option_list, cli_spacing_funcion);
    set_shared_memory(option_list, shared_context_memory);

    calendar    = build_calendar(data_chain);
    ac          = process_calendar(calendar);

    build_account_option_list(option_list, ac, function);

    free_calendar(calendar);
    free_account(ac);

    start_cli_engine(option_list);

    dystroy_cli(option_list);
}

int cli_add_transaction(void* shared_context_memory, char* option_name)
{
    if (shared_context_memory == NULL)
        return -1;

    setup_account_option_list(shared_context_memory, cli_process_add_transaction_account);

    return 0;
}

int cli_add_virt_account(void* shared_context_memory, char* option_name)
{
    if (shared_context_memory == NULL)
        return -1;

    setup_account_option_list(shared_context_memory, cli_process_add_virt_account);

    return 0;
}

int cli_delete_account(void* shared_context_memory, char* option_name)
{
    if (shared_context_memory == NULL)
        return -1;

    setup_account_option_list(shared_context_memory, cli_process_delete_account);

    return 0;
}

int cli_add_message(void* shared_context_memory, char* option_name)
{
    struct_data_chain*  data_chain          = (struct_data_chain*)shared_context_memory;
    char                name[AC_NAME_SIZE]  = { 0 };
    char                msg[1024]           = { 0 };
    struct_date         date                = { 0 };

    if (data_chain == NULL)
        return -1;

    printf("Name of sender:\n");
    get_user_text_input(name, MSG_NAME_SIZE);

    printf("Enter message:\n");
    get_user_text_input(msg, 1024);

    date = get_current_date();

    add_message_record(data_chain,
                       name,
                       date,
                       strlen(msg),
                       msg);

    return 0;
}

struct_cli_option_list* cli_print_option_list(struct_data_chain* data_chain)
{
    struct_cli_option_list* print_option_list = NULL;
    int                     option_list_index = 0;

    print_option_list = create_option_list("Select data to view");

    set_spacing_function(print_option_list, cli_spacing_funcion);
    set_shared_memory(print_option_list, (void*)data_chain);

    option_list_index = add_option_to_list(print_option_list, "Print account balances", CLI_FUNCTION_CALL);
    set_option_function(print_option_list, option_list_index, cli_print_account_balances);

    option_list_index = add_option_to_list(print_option_list, "Print account details", CLI_FUNCTION_CALL);
    set_option_function(print_option_list, option_list_index, cli_print_account_detailed);

    return print_option_list;
}

struct_cli_option_list* cli_modify_option_list(struct_data_chain* data_chain)
{
    struct_cli_option_list* modify_option_list  = NULL;
    int                     option_list_index   = 0;

    modify_option_list = create_option_list("Select data to modify");

    set_spacing_function(modify_option_list, cli_spacing_funcion);
    set_shared_memory(modify_option_list, (void*)data_chain);

    option_list_index = add_option_to_list(modify_option_list, "Add transaction", CLI_FUNCTION_CALL);
    set_option_function(modify_option_list, option_list_index, cli_add_transaction);

    option_list_index = add_option_to_list(modify_option_list, "Add account", CLI_FUNCTION_CALL);
    set_option_function(modify_option_list, option_list_index, cli_add_account);

    option_list_index = add_option_to_list(modify_option_list, "Add virtual account", CLI_FUNCTION_CALL);
    set_option_function(modify_option_list, option_list_index, cli_add_virt_account);

    option_list_index = add_option_to_list(modify_option_list, "Delete account", CLI_FUNCTION_CALL);
    set_option_function(modify_option_list, option_list_index, cli_delete_account);

    return modify_option_list;
}

struct_cli_option_list* cli_data_chain_options(struct_data_chain* data_chain)
{
    struct_cli_option_list* print_option_list = NULL;
    int                     option_list_index = 0;

    print_option_list = create_option_list("Select data chain option");

    set_spacing_function(print_option_list, cli_spacing_funcion);
    set_shared_memory(print_option_list, (void*)data_chain);

    option_list_index = add_option_to_list(print_option_list, "Print data chain", CLI_FUNCTION_CALL);
    set_option_function(print_option_list, option_list_index, cli_print_data_chain);

    option_list_index = add_option_to_list(print_option_list, "Print account records", CLI_FUNCTION_CALL);
    set_option_function(print_option_list, option_list_index, cli_print_accounts);

    option_list_index = add_option_to_list(print_option_list, "Print transaction records", CLI_FUNCTION_CALL);
    set_option_function(print_option_list, option_list_index, cli_print_transactions);

    option_list_index = add_option_to_list(print_option_list, "Add message to chain", CLI_FUNCTION_CALL);
    set_option_function(print_option_list, option_list_index, cli_add_message);

    return print_option_list;
}

int cli_print_version(void* shared_context_memory, char* option_name)
{
    char* version = get_fi_version_string();

    if (version != NULL)
    {
        printf("Financial Tracker Version: %s\n", version);
        free(version);
    }
    else
    {
        printf("Financial Tracker Version: Uknown\n");
    }

    return 0;
}

void fi_tracker_cli(struct_data_chain* data_chain)
{
    struct_cli_option_list* option_list         = NULL;
    int                     option_list_index   = 0;

    option_list = create_option_list("Financial Tracker");

    set_spacing_function(option_list, cli_spacing_funcion);
    set_shared_memory(option_list, (void*)data_chain);

    option_list_index = add_option_to_list(option_list, "Print version", CLI_FUNCTION_CALL);
    set_option_function(option_list, option_list_index, cli_print_version);

    option_list_index = add_option_to_list(option_list, "Data chain options", CLI_OPTION_LIST);
    set_option_new_option_list(option_list, option_list_index, cli_data_chain_options(data_chain));

    option_list_index = add_option_to_list(option_list, "View account data", CLI_OPTION_LIST);
    set_option_new_option_list(option_list, option_list_index, cli_print_option_list(data_chain));

    option_list_index = add_option_to_list(option_list, "Modify account data", CLI_OPTION_LIST);
    set_option_new_option_list(option_list, option_list_index, cli_modify_option_list(data_chain));

    start_cli_engine(option_list);

    dystroy_cli(option_list);
}
