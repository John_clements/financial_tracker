#include "stddef.h"
#include "stdlib.h"
#include "string.h"
#include "stdio.h"

#include "utils.h"
#include "cli.h"

void set_spacing_function(struct_cli_option_list* option_list, spacing_function spacing)
{
    if (option_list == NULL)
        return;

    option_list->spacing = spacing;
}

void set_shared_memory(struct_cli_option_list* option_list, void* shared_memory)
{
    if (option_list == NULL)
        return;

    option_list->shared_context_memory = shared_memory;
}

struct_cli_option_list* create_option_list(char* option_list_title)
{
    struct_cli_option_list* option_list = NULL;

    if (option_list_title == NULL)
        return NULL;

    option_list = (struct_cli_option_list*)malloc(sizeof(struct_cli_option_list));
    memset((void*)option_list, 0, sizeof(struct_cli_option_list));

    option_list->list_title = (char*)malloc(strlen(option_list_title)+1);
    strcpy(option_list->list_title, option_list_title);

    return option_list;
}

// Returns index of added option (error if return < 0)
int add_option_to_list(struct_cli_option_list* option_list, char* option_name, cli_option_types behaviour)
{
    char**              new_option_name_list    = NULL;
    struct_cli_option*  new_option_list         = NULL;

    if ((option_list == NULL) || (option_name == NULL))
        return -1;

    new_option_name_list = (char**)malloc((option_list->option_count+1)*sizeof(char*));
    new_option_list      = (struct_cli_option*)malloc((option_list->option_count+1)*sizeof(struct_cli_option));

    for (int i = 0; i < option_list->option_count; i++)
    {
        new_option_name_list[i] = option_list->option_name[i];
        new_option_list[i]      = option_list->option[i];
    }

    new_option_name_list[option_list->option_count] = (char*)malloc(strlen(option_name)+1);
    strcpy( new_option_name_list[option_list->option_count], option_name);

    memset((void*)&new_option_list[option_list->option_count], 0, sizeof(struct_cli_option));
    new_option_list[option_list->option_count].option_type = behaviour;

    if (option_list->option_name != NULL)
        free(option_list->option_name);
    if (option_list->option != NULL)
        free(option_list->option);

    option_list->option_name    = new_option_name_list;
    option_list->option         = new_option_list;

    option_list->option_count++;

    return option_list->option_count-1;
}

// error if return < 0
int set_option_function(struct_cli_option_list* option_list, int option_list_index, cli_function function)
{
    if (option_list == NULL)
        return -1;

    if (option_list_index > option_list->option_count)
        return -2;

    if (option_list->option[option_list_index].option_type == CLI_FUNCTION_CALL)
        option_list->option[option_list_index].call.function = function;
    else
        return -3;

    return 0;
}

// error if return < 0
int set_option_new_option_list(struct_cli_option_list* option_list, int option_list_index, struct_cli_option_list* new_option_list)
{
    if ((option_list == NULL) || (new_option_list == NULL))
        return -1;

    if (option_list_index > option_list->option_count)
        return -2;

    if (option_list->option[option_list_index].option_type == CLI_OPTION_LIST)
    {
        if (option_list->option[option_list_index].call.new_option_list == NULL)
            option_list->option[option_list_index].call.new_option_list = new_option_list;
        else
            return -3;
    }
    else
        return -4;

    return 0;
}

void print_cli_options(struct_cli_option_list* option_list)
{
    if (option_list->spacing != NULL)
        option_list->spacing();

    printf(KGRN"%s"KNRM"\n", option_list->list_title);
    printf("Select option:\n");

    for (int i = 0; i < option_list->option_count; i++)
        printf(KMAG"%d:"KNRM" %s\n", i+1, option_list->option_name[i]);

    printf(KRED"%d: Exit"KNRM"\n", option_list->option_count+1);
}

// error if return < -1
int get_user_selected_option()
{
    int input_result    = 0;
    int selection       = 0;

    selection = -1;
    printf(KGRN);
    input_result = scanf("%d", &selection);
    printf(KNRM);

    if (input_result == EOF) 
        return -1;

    if (input_result == 0)
        while (fgetc(stdin) != '\n');

    return selection;
}

// error if return < 0
int start_cli_engine(struct_cli_option_list* option_list)
{
    int ret         = 0;
    int selection    = 0;

    if (option_list == NULL)
        return -1;

    if (option_list->list_title == NULL)
        return -2;

    do
    {
        print_cli_options(option_list);

        selection = get_user_selected_option();

        if (selection < 0) 
        {
            printf("Invalid input\n");
            continue;
        }

        // Exit option
        if (selection == option_list->option_count+1)
            break;

        if ((selection > 0) && (selection < option_list->option_count+1))
        {
            ret = execute_list_option(option_list, selection-1);
            if (ret < 0)
                return ret;
        }
    } while(1);

    return 0;
}

// error if return < 0
int execute_list_option(struct_cli_option_list* option_list, int option_list_index)
{
    if (option_list == NULL)
        return -1;

    if (option_list_index > option_list->option_count)
        return -2;

    if (option_list->option[option_list_index].option_type == CLI_OPTION_LIST)
    {
        if (option_list->option[option_list_index].call.new_option_list != NULL)
            return  start_cli_engine(option_list->option[option_list_index].call.new_option_list);
    }
    else if (option_list->option[option_list_index].option_type == CLI_FUNCTION_CALL)
    {
        if (option_list->option[option_list_index].call.function != NULL)
        {
            if (option_list->spacing != NULL)
                option_list->spacing();

            return  option_list->option[option_list_index].call.function(option_list->shared_context_memory, option_list->option_name[option_list_index]);
        }
    }

    return -1;
}

int get_option_list_index(struct_cli_option_list* option_list, char* option_list_name)
{
    if ((option_list == NULL) || (option_list_name == NULL))
        return -1;

    for (int i = 0; i < option_list->option_count; i++)
    {
        if (strcmp(option_list->option_name[i], option_list_name) == 0)
        {
            if (option_list->option[i].option_type == CLI_OPTION_LIST)
                return i;
        }
    }

    return -1;
}

struct_cli_option_list* get_option_list(struct_cli_option_list* option_list, char* option_list_name)
{
    int index = 0;

    if ((option_list == NULL) || (option_list_name == NULL))
        return NULL;

    index = get_option_list_index(option_list, option_list_name);
    if (index >= 0)
        return option_list->option[index].call.new_option_list;

    return NULL;
}

// error if return < 0
int create_option_list_nest(struct_cli_option_list* option_list, char* option_list_name, char* option_list_title)
{
    int ret     = 0;
    int index   = 0;

    if ((option_list == NULL) || (option_list_name == NULL) || (option_list_title == NULL))
        return -1;

    index = get_option_list_index(option_list, option_list_name);
    if (index >= 0)
    {
        option_list->option[index].call.new_option_list = create_option_list(option_list_title);
        if (option_list->option[index].call.new_option_list != NULL)
        {
            set_spacing_function(option_list->option[index].call.new_option_list, option_list->spacing);
            set_shared_memory(option_list->option[index].call.new_option_list, option_list->shared_context_memory);
        }
        else
            ret = -2;
    }
    else
        ret = -3;

    return ret;
}

void dystroy_cli(struct_cli_option_list* option_list)
{
    if (option_list->option_name != NULL)
    {
        for (int i = 0; i < option_list->option_count; i++)
            if (option_list->option_name[i] != NULL)
                free(option_list->option_name[i]);
        free(option_list->option_name);
    }

    if (option_list->option != NULL)
    {
        for (int i = 0; i < option_list->option_count; i++)
            if (option_list->option[i].option_type == CLI_OPTION_LIST)
                if (option_list->option[i].call.new_option_list != NULL)
                    dystroy_cli(option_list->option[i].call.new_option_list);
        free(option_list->option);
    }

    if (option_list->list_title != NULL)
        free(option_list->list_title);;
}
