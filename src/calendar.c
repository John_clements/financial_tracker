#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#include "account.h"
#include "transaction.h"
#include "interpreter.h"
#include "calendar.h"

struct_date get_date_from_account_record(struct_ac_record* ac_record)
{
    struct_date date = { 0 };

    if (ac_record == NULL)
        return date;

    return ac_record->date;
}

struct_date get_date_from_virt_account_record(struct_virt_ac_record* virt_ac_record)
{
    struct_date date = { 0 };

    if (virt_ac_record == NULL)
        return date;

    return virt_ac_record->date;
}

struct_date get_date_from_transaction(struct_tr* tr)
{
    struct_date date = { 0 };

    if (tr == NULL)
        return date;

    return tr->date;
}

struct_date get_date_from_chain_link_data(void* data)
{
    struct_date date = { 0 };

    if (data == NULL)
        return date;

    if (is_ac_record_struct_valid((struct_ac_record*)data))
        return get_date_from_account_record((struct_ac_record*)data);
    else if (is_virt_ac_record_struct_valid((struct_virt_ac_record*)data))
        return get_date_from_virt_account_record((struct_virt_ac_record*)data);
    else if (is_tr_struct_valid((struct_tr*)data))
        return get_date_from_transaction((struct_tr*)data);

    return date;
}

struct_calendar_year* get_calendar_year(struct_calendar_year* calendar_year_list, uint32_t year)
{
    if (calendar_year_list == NULL)
        return NULL;

    if (calendar_year_list->year == year)
        return calendar_year_list;

    return get_calendar_year(calendar_year_list->next_year, year);
}

struct_calendar_year* init_calendar_year(uint32_t year)
{
    struct_calendar_year* new_year = (struct_calendar_year*)malloc(sizeof(struct_calendar_year));

    memset((void*)new_year, 0, sizeof(struct_calendar_year));

    new_year->year = year;

    return new_year;
}

struct_calendar_year* add_calendar_year(struct_calendar_year* calendar_year_list, uint32_t year)
{
    struct_calendar_year* year_list     = NULL;
    struct_calendar_year* new_year_list = NULL;

    if (calendar_year_list == NULL)
        return NULL;

    if (get_calendar_year(calendar_year_list, year))
        return NULL;

    year_list = calendar_year_list->next_year;

    while (year_list->next_year != NULL)
    {
        if (year_list->next_year->year > year)
            break;

        year_list = year_list->next_year;
    }

    new_year_list               = init_calendar_year(year);
    new_year_list->next_year    = year_list->next_year;

    year_list->next_year        = new_year_list;

    return new_year_list;
}

struct_calendar* build_calendar(struct_data_chain* data_chain)
{
    struct_calendar*        calendar        = NULL;
    void*                   data            = NULL;
    uint32_t                data_size       = 0;
    struct_date             date            = { 0 };
    uint32_t                calendar_index  = 0;

    struct_calendar_year*   year            = NULL;

    if (data_chain == NULL)
        return NULL;

    calendar = (struct_calendar*)malloc(sizeof(struct_calendar));
    memset((void*)calendar, 0, sizeof(struct_calendar));

    for (uint32_t i = 0; i < chain_link_count(data_chain); i++)
    {
        data = get_link_data(get_chain_link(data_chain, i), &data_size);

        memset((void*)&date, 0, sizeof(struct_date));
        date = get_date_from_chain_link_data(data);

        year = get_calendar_year(calendar->start_year, (uint32_t)date.year);

        if (year == NULL)
            year = add_calendar_year(calendar->start_year, (uint32_t)date.year);

        if (year == NULL)
        {
            year = init_calendar_year((uint32_t)date.year);

            calendar->start_year = year;
        }

        if (year)
        {
            if (year->data_chain[date.month - 1] == NULL)
                year->data_chain[date.month - 1] = init_data_chain();

            add_chain_link(year->data_chain[date.month - 1], data, data_size);
        }
        else
        {
            // Report error here
        }
    }

    return calendar;
}

void free_calendar_year_list(struct_calendar_year* calendar_year_list)
{
    if (calendar_year_list == NULL)
        return;

    free_calendar_year_list(calendar_year_list->next_year);

    for (uint32_t calendar_index = 0; calendar_index < MONTHS_IN_YEAR; calendar_index++)
        free_data_chain(calendar_year_list->data_chain[calendar_index]);

    free(calendar_year_list);
}

void free_calendar(struct_calendar* calendar)
{
    if (calendar != NULL)
        return;

    if (calendar->start_year != NULL)
        free_calendar_year_list(calendar->start_year);

    free(calendar);
}

void execute_account_rules(struct_ac* ac, enum_fill_type priority)
{
    struct_ac*  ac_parent           = NULL;
    int         balance_remainder   = 0;
    int         balance_adjustment  = 0;

    if (ac == NULL)
        return;

    // Check if ac is empty
    if (ac->name[0] == 0)
        return;

    ac_parent = ac->parent_account;

    if (ac_parent != NULL)
    {
        balance_remainder = ac_parent->balance - ac_parent->allocated_funds;

        if (balance_remainder < 0)
            balance_remainder = 0;

        if (ac->balance < ac->capacity)
        {
            if (ac->required_fill == priority)
            {
                if ((ac->required_fill == FILL_TYPE_REQUIRED) ||
                    (balance_remainder >= ac->monthly_request))
                {
                    balance_adjustment = ac->monthly_request;
                }
                else
                    balance_adjustment = balance_remainder;

                if ((ac->balance + balance_adjustment) > ac->capacity)
                    balance_adjustment = ac->capacity - ac->balance;

                ac->balance                 = ac->balance + balance_adjustment;
                ac_parent->allocated_funds  = ac_parent->allocated_funds + balance_adjustment;
            }
        }
    }

    if (ac->child_account != NULL)
        execute_account_rules(ac->child_account, priority);

    execute_account_rules(ac->next_account, priority);
}

struct_ac* process_calendar(struct_calendar* calendar)
{
    struct_ac*              ac              = NULL;
    void*                   data            = NULL;
    struct_calendar_year*   calendar_year   = NULL;

    if (calendar == NULL)
        return NULL;

    ac = init_account();

    calendar_year = calendar->start_year;

    while (calendar_year != NULL)
    {
        for (uint32_t calendar_index = 0; calendar_index < MONTHS_IN_YEAR; calendar_index++)
        {
            if (calendar_year->data_chain[calendar_index] == NULL)
                continue;

            for (uint32_t i = 0; i < chain_link_count(calendar_year->data_chain[calendar_index]); i++)
            {
                data = get_link_data(get_chain_link(calendar_year->data_chain[calendar_index], i), NULL);

                if (is_tr_struct_valid((struct_tr*)data))
                    process_tr_record(ac, (struct_tr*)data);
            }

            execute_account_rules(ac, FILL_TYPE_REQUIRED);
            execute_account_rules(ac, FILL_TYPE_NOT_REQUIRED);

            for (uint32_t i = 0; i < chain_link_count(calendar_year->data_chain[calendar_index]); i++)
            {
                data = get_link_data(get_chain_link(calendar_year->data_chain[calendar_index], i), NULL);

                if (is_ac_record_struct_valid((struct_ac_record*)data))
                    process_ac_record(ac, (struct_ac_record*)data);
                else if (is_virt_ac_record_struct_valid((struct_virt_ac_record*)data))
                    process_virt_ac_record(ac, (struct_virt_ac_record*)data);
            }
        }

        calendar_year = calendar_year->next_year;
    }

    return ac;
}
