#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#include "interpreter.h"
#include "transaction.h"
#include "utils.h"

struct_ac* init_account()
{
    struct_ac* ac = NULL;

    ac = (struct_ac*)malloc(sizeof(struct_ac));
    memset((void*)ac, 0, sizeof(struct_ac));

    return ac;
}

struct_ac* get_account(struct_ac* ac, char* name)
{
    struct_ac* ac_child = NULL;

    if ((ac == NULL) || (name == NULL))
        return NULL;

    if (strcmp(name, ac->name) == 0)
        return ac;

    if (ac->child_account != NULL)
    {
        ac_child = get_account(ac->child_account, name);

        if (ac_child != NULL)
            return ac_child;
    }

    return get_account(ac->next_account, name);
}

int does_account_exist(struct_ac* ac, char* name)
{
    if ((ac == NULL) || (name == NULL))
        return 0;

    if (get_account(ac, name) != NULL)
        return 1;

    return 0;
}

struct_ac* get_last_account(struct_ac* ac)
{
    if (ac == NULL)
        return NULL;

    while (ac->next_account != NULL)
        ac = ac->next_account;

    return ac;
}

void set_last_account(struct_ac* ac, struct_ac* ac_new)
{
    if (ac == NULL)
        return;

    ac = get_last_account(ac);

    ac->next_account = ac_new;
}

void set_last_child_account(struct_ac* ac, struct_ac* ac_child)
{
    if (ac == NULL)
        return;

    if (ac->child_account == NULL)
        ac->child_account = ac_child;
    else
        set_last_account(ac->child_account, ac_child);
}

void add_ac_account(struct_ac* ac, char* name)
{
    if ((ac == NULL) || (name == NULL))
        return;

    if (does_account_exist(ac, name))
        return;

    ac                  = get_last_account(ac);
    ac->next_account    = init_account();
    ac                  = ac->next_account;

    strncpy(ac->name, name, AC_NAME_SIZE);
}

void delete_ac_account(struct_ac* ac, char* name)
{
    struct_ac* del_ac = NULL;

    if ((ac == NULL) || (name == NULL))
        return;

    del_ac = get_account(ac, name);

    if (del_ac == NULL)
        return;

    free_account(del_ac->child_account);

    if (del_ac->parent_account == NULL)
    {
        if (del_ac == ac)
        {
            del_ac = del_ac->next_account;

            memcpy((void*)ac, (void*)ac->next_account, sizeof(struct_ac));
        }
        else
        {
            while (ac->next_account != del_ac)
            {
                ac = ac->next_account;
            }

            ac->next_account = del_ac->next_account;
        }
    }
    else
    {
        ac = del_ac->parent_account;

        ac->allocated_funds = ac->allocated_funds - del_ac->balance;

        if (del_ac == ac->child_account)
        {
            ac->child_account = del_ac->next_account;
        }
        else
        {
            ac = ac->child_account;

            while (ac->next_account != del_ac)
            {
                ac = ac->next_account;
            }

            ac->next_account = del_ac->next_account;
        }
    }

    free(del_ac);
}

void process_tr_record(struct_ac* ac, struct_tr* tr)
{
    if ((ac == NULL) || (tr == NULL))
        return;

    ac = get_account(ac, tr->account);

    if (ac != NULL)
    {
        // Propogate transaction amount to parent accounts
        do
        {
            ac->balance = ac->balance + tr->amount;

            ac = ac->parent_account;

            if (ac != NULL)
                ac->allocated_funds = ac->allocated_funds + tr->amount;

        } while (ac != NULL);
    }
    else
    {
        printf(KRED"Error"KNRM": Account "KRED"%s"KNRM" does not exist\n", tr->account);
    }
}

void process_inter_tr_record(struct_ac* ac, struct_inter_tr* tr)
{
    struct_ac* source_ac = NULL;
    struct_ac* dest_ac   = NULL;

    if ((ac == NULL) || (tr == NULL))
        return;

    source_ac = get_account(ac, tr->source_account);
    dest_ac   = get_account(ac, tr->dest_account);

    if ((source_ac != NULL) && (dest_ac != NULL))
    {
        // Propogate transaction amount to parent accounts
        do
        {
            source_ac->balance = source_ac->balance - tr->amount;

            source_ac = source_ac->parent_account;

            if (source_ac != NULL)
                source_ac->allocated_funds = source_ac->allocated_funds - tr->amount;

        } while (source_ac != NULL);

        do
        {
            dest_ac->balance = dest_ac->balance + tr->amount;

            dest_ac = dest_ac->parent_account;

            if (dest_ac != NULL)
                dest_ac->allocated_funds = dest_ac->allocated_funds + tr->amount;

        } while (dest_ac != NULL);
    }
    else
    {
        if (source_ac == NULL)
            printf(KRED"Error"KNRM": Account "KRED"%s"KNRM" does not exit\n", tr->source_account);

        if (dest_ac == NULL)
            printf(KRED"Error"KNRM": Account "KRED"%s"KNRM" does not exit\n", tr->dest_account);
    }
}

void process_ac_record(struct_ac* ac, struct_ac_record* ac_record)
{
    if ((ac == NULL) || (ac_record == NULL))
        return;

    if (ac_record->record_type == AC_RECORD_ADD)
    {
        // Check if account name is valid, if 0 then it is empty entry
        if (ac->name[0] == 0)
            strncpy(ac->name, ac_record->name, AC_NAME_SIZE);
        else
            add_ac_account(ac, ac_record->name);
    }
    else if (ac_record->record_type == AC_RECORD_REMOVE)
    {
        delete_ac_account(ac, ac_record->name);
    }
    else
    {
        // Invalid record type
    }
}

void add_child_account(struct_ac*     ac,
                       char*          name,
                       int            monthly_request,
                       int            capacity,
                       enum_fill_type required_fill)
{
    struct_ac* ac_new = NULL;

    if ((ac == NULL) || (name == NULL))
        return;

    ac_new = init_account();

    strncpy(ac_new->name, name, AC_NAME_SIZE);

    ac_new->monthly_request   = monthly_request;
    ac_new->capacity          = capacity;
    ac_new->required_fill     = required_fill;
    ac_new->parent_account    = ac;

    set_last_child_account(ac, ac_new);
}

void update_child_account(struct_ac*     ac,
                          char*          name,
                          int            monthly_request,
                          int            capacity,
                          enum_fill_type required_fill)
{
    if ((ac == NULL) || (name == NULL))
        return;

    ac = get_account(ac, name);

    if (ac != NULL)
    {
        ac->monthly_request   = monthly_request;
        ac->capacity          = capacity;
        ac->required_fill     = required_fill;
        ac->parent_account    = ac;
    }
}

void process_virt_ac_record(struct_ac* ac, struct_virt_ac_record* virt_ac_record)
{
    if ((ac == NULL) || (virt_ac_record == NULL))
        return;

    ac = get_account(ac, virt_ac_record->parent_name);

    if (ac == NULL)
    {
        // TODO: Error
        return;
    }

    if (virt_ac_record->record_type == AC_RECORD_ADD)
    {
        add_child_account(ac,
                          virt_ac_record->name,
                          virt_ac_record->monthly_request,
                          virt_ac_record->capacity,
                          virt_ac_record->required_fill);
    }
    else if (virt_ac_record->record_type == AC_RECORD_UPDATE)
    {
        update_child_account(ac,
                             virt_ac_record->name,
                             virt_ac_record->monthly_request,
                             virt_ac_record->capacity,
                             virt_ac_record->required_fill);
    }
    else if (virt_ac_record->record_type == AC_RECORD_REMOVE)
    {
        delete_ac_account(ac, virt_ac_record->name);
    }
    else
    {
        // Invalid record type
    }
}

void interpret_data(struct_ac* ac, void* data)
{
    if ((ac == NULL) || (data == NULL))
        return;

    if (is_ac_record_struct_valid((struct_ac_record*)data))
        process_ac_record(ac, (struct_ac_record*)data);
    else if (is_virt_ac_record_struct_valid((struct_virt_ac_record*)data))
        process_virt_ac_record(ac, (struct_virt_ac_record*)data);
    else if (is_tr_struct_valid((struct_tr*)data))
        process_tr_record(ac, (struct_tr*)data);
    else if (is_inter_tr_struct_valid((struct_inter_tr*)data))
        process_inter_tr_record(ac, (struct_inter_tr*)data);
}

void print_account_balance_data(struct_ac* ac, uint32_t indent)
{
    if (ac == NULL)
        return;

    print_indent(indent);
    printf("Name:            %s\n", ac->name);

    print_indent(indent);
    if (ac->balance >= 0)
        printf(KCYN"Balance"KNRM":         "KGRN"%d"KNRM"\n", ac->balance);
    else
        printf(KCYN"Balance"KNRM":         "KRED"%d"KNRM"\n", ac->balance);

    if (ac->allocated_funds > ac->balance)
    {
        print_indent(indent);
        printf(KRED"Over Allocated"KNRM":  "KRED"%d"KNRM"\n", ac->allocated_funds - ac->balance);
    }
    else
    {
        //print_indent(indent);
        //printf(KGRN"Under Allocated"KNRM": "KGRN"%d"KNRM"\n", ac->allocated_funds - ac->balance);
    }
}

void print_account_data(struct_ac* ac, uint32_t indent)
{
    if (ac == NULL)
        return;

    print_account_balance_data(ac, indent);

    if (ac->parent_account != NULL)
    {
        if (ac->capacity != AC_MAX_CAPACITY)
        {
            print_indent(indent);
            printf("Capacity:        %d\n", ac->capacity);
        }

        print_indent(indent);
        printf("Monthly Request: %d\n", ac->monthly_request);

        if (ac->required_fill == FILL_TYPE_REQUIRED)
        {
            print_indent(indent);
            printf("Required Fill:   "KGRN"Yes"KNRM"\n");
        }
    }
}

void print_account_divider(uint32_t indent)
{
    print_indent(indent);
    printf(KYEL);
    print_divider("", DIVIDER_WALL_LEN - indent*INDENT_SIZE);
    printf(KNRM);
}

void print_account_detailed(struct_ac* ac, uint32_t indent)
{
    if (ac == NULL)
        return;

    print_account_divider(indent);

    print_account_data(ac, indent);

    print_account_detailed(ac->child_account, indent + 1);
    print_account_detailed(ac->next_account,  indent);
}

void print_account_balance(struct_ac* ac, uint32_t indent)
{
    if (ac == NULL)
        return;

    print_account_divider(indent);

    print_account_balance_data(ac, indent);

    print_account_balance(ac->child_account, indent + 1);
    print_account_balance(ac->next_account,  indent);
}

void free_account(struct_ac* ac)
{
    if (ac == NULL)
        return;

    if (ac->child_account != NULL)
    {
        free_account(ac->child_account);

        free(ac->child_account);
    }

    if (ac->next_account != NULL)
    {
        free_account(ac->next_account);

        free(ac->next_account);
    }
}
