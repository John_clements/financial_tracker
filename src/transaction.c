#include "stdio.h"
#include "stdint.h"
#include "stdlib.h"
#include "string.h"

#include "transaction.h"
#include "account.h"

int is_tr_struct_valid(struct_tr* tr)
{
    if (tr == NULL)
        return 0;

    if (is_fi_header_valid(&tr->header) == 0)
        return 0;

    if (tr->header.fi_type != FI_TYPE_TRANSACTION)
        return 0;

    return 1;
}

int is_inter_tr_struct_valid(struct_inter_tr* tr)
{
    if (tr == NULL)
        return 0;

    if (is_fi_header_valid(&tr->header) == 0)
        return 0;

    if (tr->header.fi_type != FI_TYPE_INTER_TRANSACTION)
        return 0;

    return 1;
}

struct_tr* get_last_transaction(struct_data_chain* data_chain)
{
    struct_tr* tr       = NULL;
    struct_tr* tr_last  = NULL;

    if (data_chain == NULL)
        return NULL;

    for (uint32_t i = 0; i < chain_link_count(data_chain); i++)
    {
        tr = (struct_tr*)get_link_data(get_chain_link(data_chain, i), NULL);

        if (is_tr_struct_valid(tr))
            tr_last = tr;
    }

    return tr_last;
}

struct_tr* get_transaction(struct_data_chain* data_chain, uint32_t id)
{
    struct_tr* tr = NULL;

    if (data_chain == NULL)
        return NULL;

    for (uint32_t i = 0; i < chain_link_count(data_chain); i++)
    {
        tr = (struct_tr*)get_link_data(get_chain_link(data_chain, i), NULL);

        if (is_tr_struct_valid(tr))
        {
            if (tr->id == id)
                break;
        }

        tr = NULL;
    }

    return tr;
}

struct_tr* construct_transaction(char*          account,
                                 char*          desc,
                                 int            amount,
                                 struct_date    date)
{
    struct_tr* tr = (struct_tr*)malloc(sizeof(struct_tr));

    memset((void*)tr, 0, sizeof(struct_tr));

    if (desc != NULL)
        strncpy(tr->desc, desc, TR_DESC_SIZE);

    if (account != NULL)
        strncpy(tr->account, account, AC_NAME_SIZE);

    tr->amount  = amount;
    tr->date    = date;

    set_fi_header(&tr->header, FI_TYPE_TRANSACTION);

    return tr;
}

void add_transaction(struct_data_chain* data_chain,
                     char*              account,
                     char*              desc,
                     int                amount,
                     struct_date        date)
{
    struct_tr* tr       = NULL;
    struct_tr* tr_last  = NULL;

    if (data_chain == NULL)
        return;

    tr      = construct_transaction(account, desc, amount, date);
    tr_last = get_last_transaction(data_chain);

    if (tr_last == NULL)
        tr->id = 0;
    else
        tr->id = tr_last->id + 1;

    add_chain_link(data_chain, (void*)tr, sizeof(struct_tr));
}

struct_inter_tr* construct_inter_transaction(char*          source_account,
                                             char*          dest_account,
                                             char*          desc,
                                             int            amount,
                                             struct_date    date)
{
    struct_inter_tr* tr = (struct_inter_tr*)malloc(sizeof(struct_inter_tr));

    memset((void*)tr, 0, sizeof(struct_inter_tr));

    if (desc != NULL)
        strncpy(tr->desc, desc, TR_DESC_SIZE);

    if (source_account != NULL)
        strncpy(tr->source_account, source_account, AC_NAME_SIZE);

    if (dest_account != dest_account)
        strncpy(tr->source_account, dest_account, AC_NAME_SIZE);

    tr->amount  = amount;
    tr->date    = date;

    set_fi_header(&tr->header, FI_TYPE_INTER_TRANSACTION);

    return tr;
}

void add_inter_transaction(struct_data_chain* data_chain,
                           char*              source_account,
                           char*              dest_account,
                           char*              desc,
                           int                amount,
                           struct_date        date)
{
    struct_inter_tr* tr       = NULL;
    struct_tr*       tr_last  = NULL;

    if (data_chain == NULL)
        return;

    tr      = construct_inter_transaction(source_account, dest_account, desc, amount, date);
    tr_last = get_last_transaction(data_chain);

    if (tr_last == NULL)
        tr->id = 0;
    else
        tr->id = tr_last->id + 1;

    add_chain_link(data_chain, (void*)tr, sizeof(struct_inter_tr));
}

void print_transaction(struct_tr* tr)
{
    if (tr == NULL)
        return;

    PRINT_DIVIDER("");
    printf(KMAG"Transaction Record\n"KNRM);
    printf("ID:             %d\n", tr->id);
    printf("Date:           %4d-%02d-%02d\n", tr->date.year, tr->date.month, tr->date.day);
    printf("Account:        %s\n", tr->account);
    printf("Description:    %s\n", tr->desc);
    printf("Amount:         %d\n", tr->amount);
}

void print_inter_transaction(struct_inter_tr* tr)
{
    if (tr == NULL)
        return;

    PRINT_DIVIDER("");
    printf(KMAG"Inter Transaction Record\n"KNRM);
    printf("ID:             %d\n", tr->id);
    printf("Date:           %4d-%02d-%02d\n", tr->date.year, tr->date.month, tr->date.day);
    printf("Source Account: %s\n", tr->source_account);
    printf("Dest Account:   %s\n", tr->dest_account);
    printf("Description:    %s\n", tr->desc);
    printf("Amount:         %d\n", tr->amount);
}

void print_all_transactions(struct_data_chain* data_chain)
{
    void* data = NULL;

    if (data_chain == NULL)
        return;

    for (uint32_t i = 0; i < chain_link_count(data_chain); i++)
    {
        data = get_link_data(get_chain_link(data_chain, i), NULL);

        if (is_tr_struct_valid((struct_tr*)data))
            print_transaction((struct_tr*)data);
        else if (is_inter_tr_struct_valid((struct_inter_tr*)data))
            print_inter_transaction((struct_inter_tr*)data);
    }
}
