#ifndef _CLI_H_
#define _CLI_H_

typedef union  union_cli_option         union_cli_option;
typedef struct struct_cli_option        struct_cli_option;
typedef struct struct_cli_option_list   struct_cli_option_list;

// error if return < 0
typedef int  (*cli_function)(void* shared_context_memory, char* option_name);
typedef void (*spacing_function)();

typedef enum 
{
	CLI_NULL,
	CLI_OPTION_LIST,
	CLI_FUNCTION_CALL
} cli_option_types;

union union_cli_option
{
    struct_cli_option_list*  new_option_list;
	cli_function             function;
};

struct struct_cli_option
{
    cli_option_types option_type;
    union_cli_option call;
};

struct struct_cli_option_list
{
    int                 option_count;
    char*               list_title;
    char**              option_name;
    struct_cli_option*  option;
    void*               shared_context_memory;

    // Assign spacing between list segments
    spacing_function    spacing;
};

struct_cli_option_list* create_option_list(char* option_list_title);

struct_cli_option_list* get_option_list(struct_cli_option_list* option_list,
                                        char*                   option_list_name);

// error if return < 0
int create_option_list_nest(struct_cli_option_list* option_list,
                            char*                   option_list_name,
                            char*                   option_list_title);

// Returns index of added option (error if return < 0)
int add_option_to_list(struct_cli_option_list*  option_list,
                       char*                    option_name,
                       cli_option_types         behaviour);

// error if return < 0
int set_option_new_option_list(struct_cli_option_list* option_list,
                               int                     option_list_index,
                               struct_cli_option_list* new_option_list);

// error if return < 0
int set_option_function(struct_cli_option_list* option_list,
                        int                     option_list_index,
                        cli_function            function);

// error if return < 0
int start_cli_engine(struct_cli_option_list* option_list);

// error if return < 0
int execute_list_option(struct_cli_option_list* option_list,
                        int                     option_list_index);

void set_spacing_function(struct_cli_option_list* option_list,
                          spacing_function        spacing);

void set_shared_memory(struct_cli_option_list* option_list,
                       void*                   shared_memory);

void dystroy_cli(struct_cli_option_list* option_list);

#endif

