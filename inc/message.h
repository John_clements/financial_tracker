#ifndef __MESSAGE_H__
#define __MESSAGE_H__

#include "data_chain.h"
#include "utils.h"
#include "main.h"

#define MSG_NAME_SIZE 32

typedef struct struct_msg_record
{
    fi_header           header;
    char                sender_name[MSG_NAME_SIZE];
    struct_date         date;
    uint32_t            message_size;
    char*               message;
} struct_msg_record;

void add_message_record(struct_data_chain* data_chain,
                        char*              sender_name,
                        struct_date        date,
                        uint32_t           message_size,
                        char*              message);

int is_msg_record_struct_valid(struct_msg_record* msg_record);

struct_msg_record* decode_message_record(void* chain_link_data);

void print_message_record(struct_msg_record* msg_record);

#endif
