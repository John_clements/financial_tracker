#ifndef __MAIN_H__
#define __MAIN_H__

#include "stdint.h"

#include "utils.h"
#include "data_chain.h"

#define FI_TRACKER_UUID 	0x4E4E4124
#define	FI_TRACKER_VERSION	0x01041209  // 01.04.12.09

// VERSIONING: 0x00_00_00_00
//               |  |  |  |
//               |  |  |  |--- Month
//               |  |  |------ Year
//               |  |--------- Minor Version
//               |------------ Major Version

#define DIVIDER(x) print_divider((x), 64)

typedef enum
{
    FI_TYPE_UNKOWN,
    FI_TYPE_TRANSACTION,
    FI_TYPE_INTER_TRANSACTION,
    FI_TYPE_ACCOUNT,
    FI_TYPE_VIRT_ACCOUNT,
    FI_TYPE_MESSAGE
} enum_fi_type;

typedef struct fi_header
{
    uint32_t        fi_uuid;
    uint32_t        version;
    enum_fi_type    fi_type;
    uint32_t        reserve;
} fi_header;

void fi_tracker_cli(struct_data_chain* data_chain);
void print_fi_data_chain(struct_data_chain* data_chain);

int     is_fi_header_valid(fi_header* header);
void    set_fi_header(fi_header* header, enum_fi_type fi_type);
char*   get_fi_version_string();

#endif
