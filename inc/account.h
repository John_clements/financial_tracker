#ifndef __ACCOUNT_H__
#define __ACCOUNT_H__

#include "data_chain.h"
#include "utils.h"
#include "main.h"

#define AC_NAME_SIZE 32

typedef enum 
{
	FILL_TYPE_UNKOWN,
	FILL_TYPE_REQUIRED,
	FILL_TYPE_NOT_REQUIRED,
} enum_fill_type;

typedef enum
{
	AC_RECORD_UNKNOWN,
	AC_RECORD_ADD,
	AC_RECORD_UPDATE,
	AC_RECORD_REMOVE,
} enum_record_type;

typedef struct struct_ac_record
{
    fi_header           header;
    enum_record_type    record_type;
    char                name[AC_NAME_SIZE];
    struct_date         date;
} struct_ac_record;

typedef struct struct_virt_ac_record
{
    fi_header           header;
    enum_record_type    record_type;
    char                name[AC_NAME_SIZE];
    char                parent_name[AC_NAME_SIZE];
    int                 monthly_request;
    int                 capacity;
    enum_fill_type      required_fill;
    struct_date         date;
} struct_virt_ac_record;

/////////////////////////
// Function Prototypes //
/////////////////////////

void add_account(struct_data_chain* data_chain, char* name, struct_date date);
void delete_account(struct_data_chain* data_chain, char* name, struct_date date);

void add_virt_account(struct_data_chain* data_chain,
                      char*              name,
                      char*              parent_name,
                      int                monthly_request,
                      int                capacity,
                      enum_fill_type     required_fill,
                      struct_date        date);

void print_all_accounts(struct_data_chain* data_chain);
void print_account_record(struct_ac_record* ac_record);
void print_virt_account_record(struct_virt_ac_record* virt_ac_record);

int is_ac_record_struct_valid(struct_ac_record* ac_record);
int is_virt_ac_record_struct_valid(struct_virt_ac_record* virt_ac_record);
int is_account_added(struct_data_chain* data_chain, char* name);

#endif
