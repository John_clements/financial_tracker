#ifndef __UTILS_H__
#define __UTILS_H__

#include "stdint.h"

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
#define WINDOWS_BUILD
#endif

#ifdef WINDOWS_BUILD
#define NO_COLOUR
#endif

// Output colour defines
#ifdef NO_COLOUR
#define KNRM  ""
#define KRED  ""
#define KGRN  ""
#define KYEL  ""
#define KBLU  ""
#define KMAG  ""
#define KCYN  ""
#define KWHT  ""
#else
#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"
#endif

#define DIVIDER_WALL_LEN 64
#define PRINT_DIVIDER(x) print_divider((x), DIVIDER_WALL_LEN);
#define INDENT_SIZE 4

typedef struct struct_date
{
    uint16_t year;
    uint8_t  month; // 1 to 12
    uint8_t  day;   // 1 to 31
} struct_date;

/////////////////////////
// Function Prototypes //
/////////////////////////

void print_divider(char* title, uint8_t wall_len);
void print_indent(uint32_t cnt);

struct_date build_date_struct(uint16_t year, uint8_t month, uint8_t day);
struct_date get_current_date();
uint64_t    get_time_stamp(struct_date date);
uint8_t     get_days_in_month(uint16_t year, uint8_t month);

void    get_user_text_input(char* output_buf, uint32_t output_buf_size);
int     get_user_int_input();
int     get_user_yes_or_no();

// Print the byte contents at a memory address in a table format
void print_mem_table(uint8_t* mem, uint32_t mem_size, uint32_t line_width);

#endif
