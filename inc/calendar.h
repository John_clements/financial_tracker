#ifndef __CALENDAR_H__
#define __CALENDAR_H__

#include "data_chain.h"
#include "utils.h"

#define MONTHS_IN_YEAR  12

typedef struct struct_calendar_year struct_calendar_year;

typedef struct struct_calendar_year
{
    uint32_t                year;
    struct_data_chain*      data_chain[MONTHS_IN_YEAR];
    struct_calendar_year*   next_year;
} struct_calendar_year;

typedef struct struct_calendar
{
    struct_calendar_year*   start_year;
} struct_calendar;

/////////////////////////
// Function Prototypes //
/////////////////////////

struct_calendar* build_calendar(struct_data_chain* data_chain);
struct_ac*       process_calendar(struct_calendar* calendar);
void             free_calendar(struct_calendar* calendar);

#endif
