#ifndef __INTERPRETER_H__
#define __INTERPRETER_H__

#include "data_chain.h"
#include "account.h"
#include "transaction.h"

#define AC_MAX_CAPACITY 2147483647 // 2^31 - 2

typedef struct struct_ac struct_ac;

typedef struct struct_ac
{
    char            name[AC_NAME_SIZE];
    int             balance;
    int             allocated_funds;

    int             monthly_request;
    int             capacity;
    enum_fill_type  required_fill;

    struct_ac*      parent_account;
    struct_ac*      next_account;
    struct_ac*      child_account;
} struct_ac;

/////////////////////////
// Function Prototypes //
/////////////////////////

struct_ac* init_account();

void interpret_data(struct_ac* ac, void* data);

void print_account_detailed(struct_ac* ac, uint32_t indent);
void print_account_balance(struct_ac* ac, uint32_t indent);

void process_tr_record(struct_ac* ac, struct_tr* tr);
void process_ac_record(struct_ac* ac, struct_ac_record* ac_record);
void process_virt_ac_record(struct_ac* ac, struct_virt_ac_record* virt_ac_record);

void free_account(struct_ac* ac);

#endif
