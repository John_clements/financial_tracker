#ifndef __TRANSACTION_H__
#define __TRANSACTION_H__

#include "stdint.h"

#include "main.h"
#include "utils.h"
#include "data_chain.h"
#include "account.h"

#define TR_DESC_SIZE    128

typedef struct struct_tr
{
    fi_header       header;
    uint32_t        id;
    char            account[AC_NAME_SIZE];
    char            desc[TR_DESC_SIZE];
    int             amount;
    struct_date     date;
} struct_tr;

typedef struct struct_inter_tr
{
    fi_header       header;
    uint32_t        id;
    char            source_account[AC_NAME_SIZE];
    char            desc[TR_DESC_SIZE];
    int             amount;
    struct_date     date;
    char            dest_account[AC_NAME_SIZE];
} struct_inter_tr;

/////////////////////////
// Function Prototypes //
/////////////////////////

struct_tr* get_last_transaction(struct_data_chain* data_chain);
struct_tr* get_transaction(struct_data_chain* data_chain, uint32_t id);

void add_transaction(struct_data_chain* data_chain,
                     char*              account,
                     char*              desc,
                     int                amount,
                     struct_date        date);

void add_inter_transaction(struct_data_chain* data_chain,
                           char*              source_account,
                           char*              dest_account,
                           char*              desc,
                           int                amount,
                           struct_date        date);

void print_all_transactions(struct_data_chain* data_chain);
void print_transaction(struct_tr* tr);
void print_inter_transaction(struct_inter_tr* tr);

int is_tr_struct_valid(struct_tr* tr);
int is_inter_tr_struct_valid(struct_inter_tr* tr);

#endif
