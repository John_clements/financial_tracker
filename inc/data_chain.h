#ifndef __DATA_CHAIN_H__
#define __DATA_CHAIN_H__

#include "stdint.h"

typedef struct struct_chain_link struct_chain_link;

typedef struct struct_chain_link
{
    uint32_t    index;

    uint32_t    data_size;
    void*       data;

    struct_chain_link*  prev;
    struct_chain_link*  next;

} struct_chain_link;

typedef struct struct_data_chain
{
    uint32_t            count;
    struct_chain_link*  chain_start;
    struct_chain_link*  chain_end;
    struct_chain_link** chain_hash;

    // Flags
    uint8_t are_indices_up_to_date;
    uint8_t is_hash_up_to_date;
} struct_data_chain;

/////////////////////////
// Function Prototypes //
/////////////////////////

struct_data_chain* init_data_chain();

uint32_t chain_link_count(struct_data_chain* data_chain);

struct_chain_link* get_end_of_chain(struct_data_chain* data_chain);
struct_chain_link* get_chain_link(struct_data_chain* data_chain, uint32_t index);

void add_chain_link(struct_data_chain* data_chain, void* data, uint32_t data_size);

void* get_link_data(struct_chain_link* chain_link, uint32_t* data_size);

void export_data_chain(struct_data_chain* data_chain, char* path);
struct_data_chain* import_data_chain(char* path);

void free_data_chain(struct_data_chain* data_chain);

#endif